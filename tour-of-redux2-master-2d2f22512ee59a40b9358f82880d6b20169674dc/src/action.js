/***************************************
** O-rizon development
** Created by Alexy Hostetter
** 07/08/17 - 20:42
** action.js
** 2017 - All rights reserved
***************************************/

export const newColor = (i) => {
	return {
		type: "NEW_COLOR",
		i
	}
}

export const newNumber = (value) => {
	return {
		type: "NEW_NUMBER",
		value
	}
}