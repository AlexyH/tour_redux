/***************************************
 ** O-rizon development
 ** Created by Bastien Cecere
 ** 20/07/2017 - 18:27
 ** reducers.js
 ** 2017 - All rights reserved
 ***************************************/

import { combineReducers } from 'redux'
import getRandomColor from 'randomcolor'

const initialState = {
	carres: [
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		},
		{
			color: getRandomColor()
		}
	]
}

const func = (carre, aci, i, carres) => {
	if (aci == 0 && i == 0) {
		return {
			color: getRandomColor()

		}
	}
	else if (aci == 1 && i == 2)
		return {
			color: getRandomColor()
		}
	else if (aci == 2)
		return {
			color: carres[2].color
		}
	else {
		return {
			color: carre.color
		}
	}
}

const AppReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'NEW_COLOR' : {
			console.log(action.i)
			const i = action.i
			return {
				carres: state.carres.map((e, i, carres) => func(e, action.i, i, carres))
			}
		}
		case 'NEW_NUMBER' : {
			console.log(action.value)
			return {
				carres: Array.from(Array(action.value).keys()).map((e, i, carres) => {
					return {
						color: getRandomColor() //state.carres[i].color
					}
				})

				}
		}
		default:
			return state;
	}
}

const reducers = combineReducers({
	AppReducer
})

export default reducers