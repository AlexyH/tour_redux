import './styles/App.css'

import React from 'react'
import { connect } from 'react-redux'
import Carre from './Carre'
import {newColor, newNumber} from '../action'

const AppComponent = ({ carres, myColor, myNumber }) => {
	const styles = {
		display: 'flex',
		flexDirection: 'column',
		height: '100%',
		margin: '0 0'
	}
	const stylesTab = {
		display: 'flex',
		flexDirection: 'row',
		flex: '1 1',
		height: '50%'
	}
	var test = "HelloWorld";
	var tmp = (e) => {
		console.log(e.target.value)
	}
	var tmp2 = (e) => {
		console.log("caca")
	}

	return (
		<div style={styles}>
			<input value="4" onChange={(e)=>myNumber(e.target.value)}></input>
			{
				Array.from(Array(carres.length / 2).keys()).map((_, i) => {
					const tab = carres.map((e, i) => {
						return (
							<Carre key={'carre' + i} onClick={()=>myColor(i)} color={e.color}>{e.color}</Carre>
						)});
					return (
						<div key={'subdivider' + i} style={stylesTab}>
							{
								tab.slice(i * 2, i * 2 + 2)
							}
						</div>
					)
				})
			}
		</div>
	)
}

const mapStateToProps = ({ AppReducer }) => ({
	...AppReducer
})

const mapDispatchToProps = (dispatch) => ({
		myColor: (i) => {dispatch(newColor(i))},
		myNumber: (value) => dispatch(newNumber(value))
	}
)

const App = connect(
	mapStateToProps,
	mapDispatchToProps
)(AppComponent);

export default App
